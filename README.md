# RL agents for a multiplayer snake game

*A two man team project made for the course 'Advanced Practical Machine Learning' at the Hebrew University.* 

*	Two RL agents(policies) that learn to play a multiplayer version of the game snake, somewhat similar to the game slither.io
*	A linear agent which uses Q learning with a linear function approximation
*	a custom agent which is implemented by a Deep Q-Network(DQN) with memory replay, dropout regularization, dynamic batch size scaling and Q freezing.

The code of the game itself is not included since it was supplied by the university.

Further details can be found in the accompanying [report](https://gitlab.com/Hanan_Levin/snake-RL-agents/-/blob/master/report.pdf).

Relevant background information on the algorithms can be found here:
https://jonathanfiat.github.io/ApproxiPong/
	

