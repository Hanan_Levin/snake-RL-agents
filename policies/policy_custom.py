from collections import deque
from policies import base_policy as bp
import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Dropout, Conv2D, MaxPooling2D, Flatten
from keras.optimizers import Adam
import random

DISCOUNT = 0.9
LEARNING_RATE = 0.001

EPSILON = 1
EPSILON_MIN = 0.01
EPSILON_DECAY = 0.995

INTERPOLATION = 0.125
FREEZE_UPDATE = 50

BATCH_SIZE = 30
MEMORY_BUFFER_SIZE = 1000

MAX_RADIUS_SIZE = 4
FEATURES_PER_OBJECT = 5
MIN_BOARD_VALUE = -1
MAX_BOARD_VALUE = 9
FEATURE_VECTOR_SIZE = len(range(MIN_BOARD_VALUE, MAX_BOARD_VALUE + 1)) * FEATURES_PER_OBJECT

MODEL = 1
FREEZE_MODEL = 2


class Custom(bp.Policy):
    """
    This policy is implemented using a DQN model, which uses Q freezing and memory replay.
    The network uses dropouts for stabilization.
    """

    def init_model(self):
        """
        initialize the neural network model.
        :return: the compiled model.
        """

        model = Sequential()
        model.add(Conv2D(32, kernel_size=(3, 3), strides=(1, 1),
                         activation='relu',
                         input_shape=(MAX_RADIUS_SIZE * 2 + 1, MAX_RADIUS_SIZE * 2 + 1)))
        model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
        model.add(Conv2D(64, (5, 5), activation='relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Flatten())
        model.add(Dense(1000, activation='relu'))
        model.add(Dense(1))
        decay_rate = self.learning_rate / (self.game_duration - self.score_scope)
        model.compile(loss="mean_squared_error", optimizer=Adam(lr=self.learning_rate, decay=decay_rate))

        return model

    def cast_string_args(self, policy_args):
        policy_args['epsilon'] = float(policy_args['epsilon']) if 'epsilon' in policy_args else EPSILON
        policy_args['learning_rate'] = float(
            policy_args['learning_rate']) if 'learning_rate' in policy_args else LEARNING_RATE
        policy_args['discount'] = float(policy_args['discount']) if 'discount' in policy_args else DISCOUNT
        policy_args['batch_size'] = float(policy_args['batch_size']) if 'batch_size' in policy_args else BATCH_SIZE
        policy_args['interpolation'] = float(policy_args['interpolation']) if 'interpolation' in policy_args else \
            INTERPOLATION
        policy_args['freeze_update'] = float(
            policy_args['freeze_update']) if 'freeze_update' in policy_args else FREEZE_UPDATE
        policy_args['radius'] = float(policy_args['radius']) if 'radius' in policy_args else MAX_RADIUS_SIZE
        return policy_args

    def init_run(self):
        # self.feature_ext = FeatureExt(board_size=self.board_size)
        self.r_sum = 0
        self.memory_buffer = deque(maxlen=MEMORY_BUFFER_SIZE)
        self.model = self.init_model()
        self.frozen_model = self.init_model()

    def get_model_prediction(self, model_name, board, head, action):
        """
        return the prediction of model 'model_name' given the state of the gave an the action.
        :param model_name: MODEL or FROZEN_MODEL according to the model you want to predict with.
        :param dict_repr: a dictionary representation of the board
        :param head: the head of the snake
        :param action: the action to take
        :return: the prediction of the model.
        """
        cur_model = self.model if model_name == MODEL else self.frozen_model
        # feature_state = self.feature_ext.get_features(dict_repr, head, action)
        features = self.get_features(head, board, action)
        return cur_model.predict(features)[0]

    def get_features(self, head, board, action):
        head_pos = head[0].pos
        head_row = head_pos[0]
        head_column = head_pos[1]
        row_len = board.shape[0]
        column_len = board.shape[1]
        radius_size = int(min(MAX_RADIUS_SIZE, min((row_len - 1) / 2, (column_len - 1) / 2)))
        for row in range(head_row - radius_size, head_row + radius_size):
            for column in range(head_column - radius_size, head_column + radius_size):
                real_row = row % row_len
                real_column = column % column_len
                game_object = board[real_row][real_column]
                dict_repr[game_object].append((real_row, real_column))
        return dict_repr

    def get_max_action(self, model_name, dict_repr, head):
        """
        get the actions which yields the highest prediction by the model given
        :param model_name: MODEL or FROZEN_MODEL according to the model you want to predict with.
        :param dict_repr: a dictionary representation of the board
        :param head: the head of the snake
        :return: the action the yields the highest prediction by the model.
        """
        shuffled_actions = np.random.permutation(self.ACTIONS)  # randomize in case more than one action is highest.
        return shuffled_actions[
            np.argmax([self.get_model_prediction(model_name, dict_repr, head, action) for action in shuffled_actions])]

    def memorize_to_the_buffer(self, prev_feature_state, action, reward, new_dict_repr, new_head):
        """
        add an observation to the memory buffer.
        """
        self.memory_buffer.append([prev_feature_state, action, reward, new_dict_repr, new_head])

    def learn(self, round, prev_state, prev_action, reward, new_state, too_slow):
        if too_slow and round > 100:
            self.batch_size = max(self.batch_size - 2, 1)

        if round % 500 == 0:
            self.batch_size += 1
        try:  # todo delete
            if round % 100 == 0:
                if round > self.game_duration - self.score_scope:
                    self.log("Rewards in last 100 rounds which counts towards the score: " + str(self.r_sum), 'VALUE')
                else:
                    self.log("Rewards in last 100 rounds: " + str(self.r_sum), 'VALUE')
                self.r_sum = 0
            else:
                self.r_sum += reward

        except Exception as e:
            self.log("Something Went Wrong...", 'EXCEPTION')
            self.log(e, 'EXCEPTION')

        batch = min(self.batch_size, len(self.memory_buffer))

        samples = random.sample(self.memory_buffer, batch)
        feature_lst = []
        frozen_lst = []
        for sample in samples:
            sample_prev_feature_state, sample_action, sample_reward, sample_new_dict_repr, sample_new_head = sample

            frozen_model_evaluation = max(
                [self.get_model_prediction(FREEZE_MODEL, sample_new_dict_repr, sample_new_head,
                                           new_action)[0] for new_action in self.ACTIONS])
            frozen_prediction = sample_reward + frozen_model_evaluation * self.discount
            feature_lst.append(sample_prev_feature_state)
            frozen_lst.append(frozen_prediction)
        self.model.fit(np.array(feature_lst).reshape(batch, FEATURE_VECTOR_SIZE), np.array(
            frozen_lst).reshape(batch, 1), epochs=1, verbose=0, batch_size=batch)
        if round % FREEZE_UPDATE == 0:
            self.update_frozen_model()

    def act(self, round, prev_state, prev_action, reward, new_state, too_slow):
        if too_slow:
            self.batch_size = max(self.batch_size - 2, 1)

        if round < 2:
            return np.random.choice(bp.Policy.ACTIONS)

        self.decay_params(round)

        prev_board, prev_head = prev_state
        new_board, new_head = new_state

        new_dict_repr = self.feature_ext.build_dict_repr(new_board, new_head)
        prev_dict_repr = self.feature_ext.build_dict_repr(prev_board, prev_head)
        prev_feature_state = self.feature_ext.get_features(prev_dict_repr, prev_head, prev_action)

        self.memorize_to_the_buffer(prev_feature_state, prev_action, reward, new_dict_repr, new_head)

        if np.random.rand() < self.epsilon:
            return np.random.choice(bp.Policy.ACTIONS)

        new_action = self.get_max_action(MODEL, new_dict_repr, new_head)
        return new_action

    def update_frozen_model(self):
        """
        update the frozen model using interpolation between the current model and the current frozen model.
        """
        model_weights = self.model.get_weights()
        frozen_model_weights = self.frozen_model.get_weights()
        for i in range(len(frozen_model_weights)):
            frozen_model_weights[i] = model_weights[i] * self.interpolation + frozen_model_weights[i] * (
                    1 - self.interpolation)

        self.frozen_model.set_weights(frozen_model_weights)

    def decay_params(self, cur_round):
        """
        decay the learning rate
        """
        if cur_round > self.game_duration - self.score_scope:
            self.epsilon = 0
        else:
            self.epsilon = np.maximum(EPSILON_MIN, self.epsilon * EPSILON_DECAY)