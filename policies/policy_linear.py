from policies import base_policy as bp
import numpy as np
import time

EPSILON = 1
EPSILON_DECAY = 0.995
MIN_EPSILON = 0.01
LEARNING_RATE = 0.1
DISCOUNT = 0.9
FEATURES_PER_OBJECT = 4
MIN_OBEJECT_NUM = -1
MAX_OBEJECT_NUM = 9
FEATURE_VECTOR_SIZE = len(range(MIN_OBEJECT_NUM, MAX_OBEJECT_NUM + 1)) * FEATURES_PER_OBJECT
MAX_RADIUS_SIZE = 3


class Linear(bp.Policy):

    def cast_string_args(self, policy_args):
        policy_args['epsilon'] = float(policy_args['epsilon']) if 'epsilon' in policy_args else EPSILON
        policy_args['learning_rate'] = float(
            policy_args['learning_rate']) if 'learning_rate' in policy_args else LEARNING_RATE
        policy_args['discount'] = float(policy_args['discount']) if 'discount' in policy_args else DISCOUNT
        policy_args['radius'] = float(policy_args['radius']) if 'radius' in policy_args else MAX_RADIUS_SIZE

        return policy_args

    def init_run(self):
        self.feature_ext = FeatureExt(board_size=self.board_size)
        self.theta = np.ones(self.feature_ext.feature_vector_size) / self.feature_ext.feature_vector_size
        self.r_sum = 0

    def Q(self, dict_repr, head, action):
        """
        compute the Q value of the state (dict_repr and head position) and the action using a linear computation
        with the weight vector selt.theta.
        """
        f = self.feature_ext.get_features(dict_repr, head, action)
        return f, np.inner(self.theta, f)

    def get_max_action(self, dict_repr, head):
        """
        return the action that gives the highest Q value.
        """
        shuffled_actions = np.random.permutation(self.ACTIONS)
        features_and_qs = np.array([np.array(self.Q(dict_repr, head, action)) for action in shuffled_actions])
        argmax_q = np.argmax(features_and_qs[:, 1])
        return features_and_qs[argmax_q, 0], shuffled_actions[argmax_q]

    def learn(self, round, prev_state, prev_action, reward, new_state, too_slow):
        try:  # todo delete
            if round % 100 == 0:
                if round > self.game_duration - self.score_scope:
                    self.log("Rewards in last 100 rounds which counts towards the score: " + str(self.r_sum), 'VALUE')
                else:
                    self.log("Rewards in last 100 rounds: " + str(self.r_sum), 'VALUE')
                self.r_sum = 0
            else:
                self.r_sum += reward

        except Exception as e:
            self.log("Something Went Wrong...", 'EXCEPTION')
            self.log(e, 'EXCEPTION')

        prev_board, prev_head = prev_state
        new_board, new_head = new_state

        prev_dict_repr = self.feature_ext.build_dict_repr(prev_board, prev_head)
        new_dict_repr = self.feature_ext.build_dict_repr(new_board, new_head)

        new_feature_vector, max_next_action = self.get_max_action(new_dict_repr, new_head)
        prev_feature_vector, prev_q = self.Q(prev_dict_repr, prev_head, prev_action)
        delta_theta = prev_q - (reward + self.discount * np.inner(self.theta, new_feature_vector))
        self.theta = self.theta - self.learning_rate * delta_theta * prev_feature_vector
        self.theta /= np.sum(self.theta)
        self.decay_params(round, self.game_duration)

    def act(self, round, prev_state, prev_action, reward, new_state, too_slow):
        if np.random.rand() < self.epsilon:
            return np.random.choice(bp.Policy.ACTIONS)
        new_board, new_head = new_state
        dict_repr = self.feature_ext.build_dict_repr(new_board, new_head)
        a = self.get_max_action(dict_repr, new_head)[1]
        return a

    def decay_params(self, cur_round, rounds):
        """
        decay the epsilon parameter and the learning rate.
        """
        if cur_round < self.game_duration - self.score_scope:
            self.epsilon *= max(MIN_EPSILON, self.epsilon * EPSILON_DECAY)
            self.learning_rate *= (rounds - cur_round) / rounds
        else:
            self.epsilon = 0
            self.learning_rate = 0


class FeatureExt:
    """
    This class is used to extract a feature representation from the game board.
    """

    def __init__(self, board_size):
        self.board_size = board_size
        self.feature_vector_size = FEATURE_VECTOR_SIZE
        self.feature_vector = np.zeros(self.feature_vector_size)

    def build_dict_repr(self, board, head):
        """
        create a dictionary of the objects in that are in a square with sides with length  2*MAX_RADIUS_SIZE +1
        surrounding the head of the snake.
        The keys of the dictionary are the game objects.
        The values are all distances form the head of the objects with the same type as the key.
        """
        dict_repr = {game_object: [] for game_object in range(MIN_OBEJECT_NUM, MAX_OBEJECT_NUM + 1)}
        head_pos = head[0].pos
        head_row = head_pos[0]
        head_column = head_pos[1]
        row_len = board.shape[0]
        column_len = board.shape[1]
        radius_size = int(min(MAX_RADIUS_SIZE, min((row_len - 1) / 2, (column_len - 1) / 2)))
        for row in range(head_row - radius_size, head_row + radius_size):
            for column in range(head_column - radius_size, head_column + radius_size):
                real_row = row % row_len
                real_column = column % column_len
                game_object = board[real_row][real_column]
                dict_repr[game_object].append((real_row, real_column))
        return dict_repr

    def manhattan_distance(self, position1, position2):
        """
        compute the (cyclic) manhattan distance of p1 and p2.
        """
        man_position1 = position1[0] + 1, position1[1] + 1
        man_position2 = position2[0] + 1, position2[1] + 1
        x_distance = min(abs(man_position1[0] - man_position2[0]),
                         self.board_size[0] - man_position1[0] + man_position2[0], self.board_size[0] +
                         man_position1[0] - man_position2[0])
        y_distance = min(abs(man_position1[1] - man_position2[1]),
                         self.board_size[1] - man_position1[1] + man_position2[1], self.board_size[1] +
                         man_position1[1] - man_position2[1])
        return x_distance + y_distance

    def get_features(self, dict_repr, head, action):
        """"
        Get the a feature vector representation of the board.
        A full explanation of the features exists in the Answers.pdf.
        """

        head_pos, direction = head
        new_head_pos = head_pos.move(bp.Policy.TURNS[direction][action])

        for game_object in dict_repr.keys():
            start_obj_idx = (game_object + 1)
            self.feature_vector[start_obj_idx], self.feature_vector[start_obj_idx + 1], self.feature_vector[
                start_obj_idx + 2] = 0, 0, 0
            # bias
            self.feature_vector[start_obj_idx + 3] = 0

            if dict_repr[game_object]:
                self.feature_vector[start_obj_idx + 3] = 1
                for obj_pos in dict_repr[game_object]:
                    distance = self.manhattan_distance(new_head_pos.pos, obj_pos)
                    if distance == 0:
                        self.feature_vector[start_obj_idx] = 1
                    if distance == 1:
                        self.feature_vector[start_obj_idx + 1] = 1
                    self.feature_vector[start_obj_idx + 2] += np.square(1 / (distance + 1))
        return self.feature_vector
